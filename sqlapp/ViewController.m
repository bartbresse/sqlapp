//
//  ViewController.m
//  sqlapp
//
//  Created by Bart Breunesse on 05-10-13.
//  Copyright (c) 2013 Bart Breunesse. All rights reserved.
//

#import "ViewController.h"
#import "Author.h"
#import <sqlite3.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize theauthors, Tableview;

- (NSMutableArray *) authorlist
{
    theauthors = [[NSMutableArray alloc] initWithCapacity:10];
    @try {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:@"app.sqlite"];
        BOOL success = [fileMgr fileExistsAtPath:dbPath];
        if(!success)
        {
            NSLog(@"cannot locate database file '%@'.",dbPath);
        }
        if(!(sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK))
        {
            NSLog(@"an error has occured '%s'.", sqlite3_errmsg(db));
        }
        
        //NSLog(@"1problem with prepare statement: '%s'.", sqlite3_errmsg(db));
        
        const char *sql = "SELECT * FROM books";
        sqlite3_stmt *sqlStatement;
        if(sqlite3_prepare(db, sql, -1, &sqlStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"problem with prepare statement: '%s'.", sqlite3_errmsg(db));
        }
        else
        {
            while (sqlite3_step(sqlStatement) == SQLITE_ROW)
            {
                Author * author = [[Author alloc] init];
                author.name = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 1)];
                author.title = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 2)];
                author.genre = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 3)];
                [theauthors addObject:author];
            }
        }
    }
    @catch (NSException *exception) {
    
        NSLog(@"problem with prepare statement: %s",sqlite3_errmsg(db));
    }
    @finally {
        //sqlite3_finalize(sqlStatement);
        sqlite3_close(db);
        return theauthors;
    }
    
}

- (void)viewDidLoad
{
    [self authorlist];
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
