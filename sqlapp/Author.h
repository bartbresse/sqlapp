//
//  Author.h
//  sqlapp
//
//  Created by Bart Breunesse on 05-10-13.
//  Copyright (c) 2013 Bart Breunesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Author : NSObject
{
    NSString *name;
    NSString *title;
    NSString *genre;
}

@property( nonatomic, copy) NSString *name;
@property( nonatomic, copy) NSString *title;
@property( nonatomic, copy) NSString *genre;

@end
