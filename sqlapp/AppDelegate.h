//
//  AppDelegate.h
//  sqlapp
//
//  Created by Bart Breunesse on 05-10-13.
//  Copyright (c) 2013 Bart Breunesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
