//
//  ViewController.h
//  sqlapp
//
//  Created by Bart Breunesse on 05-10-13.
//  Copyright (c) 2013 Bart Breunesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ViewController : UIViewController
{
    NSMutableArray *theauthors;
    sqlite3 * db;
}
@property( nonatomic, retain) NSMutableArray *theauthors;

-(NSMutableArray *) authorlist;

@property (weak, nonatomic) IBOutlet UITableView *Tableview;

@end
